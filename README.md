## E-user 8.1.0 OPM1.171019.019 10or_E_V1_0_109 release-keys
- Manufacturer: 10or
- Platform: msm8937
- Codename: E
- Brand: 10or
- Flavor: lineage_E-userdebug
- Release Version: 11
- Id: RD2A.211001.002
- Incremental: eng.cirrus.20211112.085145
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SD1A.210817.036/7805805:user/release-keys
- OTA version: 
- Branch: E-user-8.1.0-OPM1.171019.019-10or_E_V1_0_109-release-keys
- Repo: 10or_e_dump_23846


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
